toolstudio.submitHandler = e => {
	const xhr = new XMLHttpRequest();
	xhr.responseType = 'blob';
	xhr.open('POST', toolstudio.url);
	xhr.onload = () => {
		const filename = xhr.getResponseHeader('Content-Disposition');
		const url = window.URL.createObjectURL(xhr.response);

		const link = document.createElement('a');
		link.href = url;
		link.innerText = `${filename} - (${xhr.response.size/1000} KB)`;
		link.download = filename;

		const linkContainer = document.getElementById('link-container');
		linkContainer.appendChild(link);
	};
	xhr.send(e.body);
}
