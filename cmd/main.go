package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"path/filepath"
	"strconv"

	"github.com/disintegration/imaging"
)

func parseFormat(format string) (imaging.Format, string) {
	switch format {
	case "jpeg", "jpg":
		return imaging.JPEG, "image/jpeg"
	case "png":
		return imaging.PNG, "image/png"
	case "bmp":
		return imaging.BMP, "image/bmp"
	case "gif":
		return imaging.GIF, "image/gif"
	case "tiff":
		return imaging.TIFF, "image/tiff"
	default:
		return imaging.JPEG, "image/jpeg"
	}
}

type info struct {
	Width        int    `json:"width,omitempty"`
	Height       int    `json:"height,omitempty"`
	OutputFormat string `json:"output_format,omitempty"`
}

func parseInfo(v url.Values) info {
	result := info{}
	if width, err := strconv.Atoi(v.Get("width")); err == nil {
		result.Width = width
	}
	if height, err := strconv.Atoi(v.Get("height")); err == nil {
		result.Height = height
	}
	result.OutputFormat = v.Get("output_format")
	return result
}

type failure struct {
	Key     string `json:"key,omitempty"`
	Message string `json:"message,omitempty"`
}

func writeError(writer http.ResponseWriter, key, msg string) {
	writer.WriteHeader(400)
	json.NewEncoder(writer).Encode(&failure{
		Key:     key,
		Message: msg,
	})
}

func handle(writer http.ResponseWriter, request *http.Request) {
	if request.Body != nil {
		defer request.Body.Close()
	}

	if err := request.ParseMultipartForm(10 << 20); err != nil {
		writeError(writer, "request-format", "Failed to parse request body.")
		return
	}

	ask := parseInfo(request.Form)

	file, header, err := request.FormFile("input-image")
	if err != nil {
		writeError(writer, "invalid-input", "Failed to retrieve image.")
		return
	}

	image, err := imaging.Decode(file)
	if err != nil {
		writeError(writer, "parser-error", "Failed to parse image. Invalid image format.")
		return
	}

	format, mimeType := parseFormat(ask.OutputFormat)
	result := imaging.Resize(image, ask.Width, ask.Height, imaging.Lanczos)
	filename := header.Filename
	var extension = filepath.Ext(filename)
	var name = filename[0 : len(filename)-len(extension)]
	writer.Header().Add("Content-Type", mimeType)
	writer.Header().Add("Content-Disposition", fmt.Sprintf("%s-resized.%s", name, mimeType[6:]))
	imaging.Encode(writer, result, format)
}

func main() {
	http.HandleFunc("/", handle)
	log.Fatalln(http.ListenAndServe(":4000", nil))
}
