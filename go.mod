module toolstudio/image-resize

go 1.16

require (
	github.com/disintegration/imaging v1.6.2
	github.com/rs/cors v1.8.0
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
)
